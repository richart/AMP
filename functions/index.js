const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const express = require('express');
const engines = require('consolidate');

const firebaseApp = firebase.initializeApp(
    functions.config().firebase
)

const app = express();

app.engine('pug', engines.pug);
app.set('views','./views');
app.set('view engine', 'pug')
app.use(express.static('public'))

app.get('/', (request, response) => {
    response.set('Cache-Control','public, max-age=300, s-max-age=600')
    response.render('index', { title: 'AMP' });
})

exports.app = functions.https.onRequest(app);
